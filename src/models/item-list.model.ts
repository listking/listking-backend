import {Entity, model, property, hasMany} from '@loopback/repository';
import {Item} from './item.model';

@model()
export class ItemList extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: false,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  color?: string;

  @hasMany(() => Item)
  items: Item[];

  @property({
    type: 'string',
  })
  userId?: string;

  constructor(data?: Partial<ItemList>) {
    super(data);
  }
}

export interface ItemListRelations {
  // describe navigational properties here
}

export type ItemListWithRelations = ItemList & ItemListRelations;
