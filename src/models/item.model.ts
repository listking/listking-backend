import {Entity, model, property, belongsTo} from '@loopback/repository';
import {ItemList, ItemListWithRelations} from './item-list.model';

@model()
export class Item extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'boolean',
    default: false,
  })
  isComplete?: boolean;

  @belongsTo(() => ItemList)
  itemListId: number;

  constructor(data?: Partial<Item>) {
    super(data);
  }
}

export interface ItemRelations {
  // describe navigational properties here
  itemList?: ItemListWithRelations;
}

export type ItemWithRelations = Item & ItemRelations;
