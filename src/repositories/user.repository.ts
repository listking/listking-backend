import {Getter, inject} from '@loopback/core';
// import {DefaultCrudRepository, HasOneRepositoryFactory, juggler, repository} from '@loopback/repository';
import {
  DefaultCrudRepository,
  HasOneRepositoryFactory,
  repository,
  HasManyRepositoryFactory,
} from '@loopback/repository';
import {User, UserCredentials, UserRelations, ItemList} from '../models';
import {UserCredentialsRepository} from './user-credentials.repository';
import {DbDataSource} from '../datasources';
import {ItemListRepository} from './item-list.repository';

export type Credentials = {
  email: string;
  password: string;
  role?: string;
};

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id,
  UserRelations
> {
  public readonly userCredentials: HasOneRepositoryFactory<
    UserCredentials,
    typeof User.prototype.id
  >;

  public readonly itemLists: HasManyRepositoryFactory<
    ItemList,
    typeof User.prototype.id
  >;

  constructor(
    //     @inject('datasources.mysql') dataSource: juggler.DataSource,
    @inject('datasources.db') dataSource: DbDataSource,
    @repository.getter('UserCredentialsRepository')
    protected userCredentialsRepositoryGetter: Getter<
      UserCredentialsRepository
    >,
    @repository.getter('ItemListRepository')
    protected itemListRepositoryGetter: Getter<ItemListRepository>,
  ) {
    super(User, dataSource);
    this.itemLists = this.createHasManyRepositoryFactoryFor(
      'itemLists',
      itemListRepositoryGetter,
    );
    this.registerInclusionResolver(
      'itemLists',
      this.itemLists.inclusionResolver,
    );
    this.userCredentials = this.createHasOneRepositoryFactoryFor(
      'userCredentials',
      userCredentialsRepositoryGetter,
    );
    this.registerInclusionResolver(
      'userCredentials',
      this.userCredentials.inclusionResolver,
    );
  }

  async findCredentials(
    userId: typeof User.prototype.id,
  ): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get();
    } catch (err) {
      if (err.code === 'ENTITY_NOT_FOUND') {
        return undefined;
      }
      throw err;
    }
  }
}
