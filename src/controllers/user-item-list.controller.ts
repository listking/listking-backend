import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {User, ItemList} from '../models';
import {UserRepository} from '../repositories';

export class UserItemListController {
  constructor(
    @repository(UserRepository) protected userRepository: UserRepository,
  ) {}

  @get('/users/{id}/item-lists', {
    responses: {
      '200': {
        description: 'Array of User has many ItemList',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(ItemList)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<ItemList>,
  ): Promise<ItemList[]> {
    return this.userRepository.itemLists(id).find(filter);
  }

  @post('/users/{id}/item-lists', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {'application/json': {schema: getModelSchemaRef(ItemList)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof User.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ItemList, {
            title: 'NewItemListInUser',
            exclude: ['id'],
            optional: ['userId'],
          }),
        },
      },
    })
    itemList: Omit<ItemList, 'id'>,
  ): Promise<ItemList> {
    return this.userRepository.itemLists(id).create(itemList);
  }

  @patch('/users/{id}/item-lists', {
    responses: {
      '200': {
        description: 'User.ItemList PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ItemList, {partial: true}),
        },
      },
    })
    itemList: Partial<ItemList>,
    @param.query.object('where', getWhereSchemaFor(ItemList))
    where?: Where<ItemList>,
  ): Promise<Count> {
    return this.userRepository.itemLists(id).patch(itemList, where);
  }

  @del('/users/{id}/item-lists', {
    responses: {
      '200': {
        description: 'User.ItemList DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(ItemList))
    where?: Where<ItemList>,
  ): Promise<Count> {
    return this.userRepository.itemLists(id).delete(where);
  }
}
