# List King API Backend

### What is this?

This is the backend for the List King application, based on Jon Duckett's "JavaScript & JQuery". It was built with Loopback's todo-tutorial (https://loopback.io/doc/en/lb4/todo-tutorial.html), todo-list-tutorial and authentication-tutorial and used scaffolding to create most project files.

### to do

- learn how to serve on different port
- activate HTTPS (https://loopback.io/doc/en/lb4/Enabling-https.html)
- enable refresh tokens (https://loopback.io/doc/en/lb4/JWT-authentication-extension.html)

### install

```bash
git clone -b local git@bitbucket.org:listking/listking-backend.git
cd listking-backend
npm install
```

### start server

```bash
cd listking-backend
npm start
firefox http://localhost:3000/explorer # check out the API-explorer
```

### requests

```bash
http http://localhost:3000/hello
http --auth-type=jwt --auth=$TOKEN http://localhost:3000/item-lists/3/items
```
